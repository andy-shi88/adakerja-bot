const request = require("request");

class FacebookService {
  /**
   * Facebook service class
   */
  constructor() {}

  /**
   * webhook token verification to match with our configuration
   * @param hubToken request[hub.verify_token] from facebook
   * @param facebookVerifyToken our local configured facebook token
   * @return true for correct token, and otherwise.
   */
  verifyToken(hubToken, facebookVerifyToken) {
    return hubToken === facebookVerifyToken ? true : false;
  }

  /**
   * send message from bot
   * @param {string} receiverId destination id
   * @param {string} message message to send
   * @param {string} pageAccessToken facebook page access token from facebook app dashboard
   * @return {mixed}
   */
  sendMessage(receiverId, message, pageAccessToken) {
    request(
      {
        url: "https://graph.facebook.com/v2.6/me/messages",
        qs: { access_token: pageAccessToken },
        method: "POST",
        json: {
          recipient: { id: receiverId },
          message: { text: message }
        }
      },
      function(error, response) {
        if (error) {
          console.log("Error sending message: ", error);
        } else if (response.body.error) {
          console.log("Error: ", response.body.error);
        }
      }
    );
  }
}

module.exports = FacebookService;
