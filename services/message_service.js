const BaseService = require("./base_service");
const models = require("../models");

class MessageService extends BaseService {
  /**
   * Message specific service class
   */

  constructor() {
    super(models.Message);
  }
}

module.exports = MessageService;
