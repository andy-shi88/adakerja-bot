const models = require("../models");
const moment = require("moment");

const {
  ASK_WELCOME_NAME,
  ASK_BIRTHDAY,
  ASK_DAYS_UNTIL_BIRTHDAY,
  RESPONSE_BYE
} = require("../constants/messages");

const {
  calculateDaysUntilBirthday,
  validateDate
} = require("../utils/calendar");

class ChatService {
  /**
   * Chat service class
   */

  constructor() {}

  /**
   * get last message to reply
   * @param {string} uid sender id
   * @return {Message} last message object
   */
  async getLastMessage(uid) {
    return models.Message.findOne({
      where: { uid },
      order: [["created_at", "DESC"]]
    });
  }

  /**
   * update last message response, and create new bot-response
   * @param {integer} lastMessageId last message id to be updated
   * @param {string} response response from user
   * @param {string} message new message to send to user
   * @param {string} uid facebook user id
   * @return {void}
   */
  async updateAndCreateResponse(lastMessageId, response, message, uid) {
    await models.Message.update(
      {
        response: message
      },
      {
        where: { id: lastMessageId }
      }
    );
    await models.Message.create({
      message: response,
      uid
    });
  }

  /**
   * reply to last unreplied message
   * @param {Message} lastMessage last message model object
   * @param {string} message user response
   * @param {User} user replying user model object
   * @return {string} response from bot
   */
  async replyLastMessage(lastMessage, message, user) {
    try {
      const response = await this.checkResponse(
        user,
        message,
        lastMessage.message
      );
      this.updateAndCreateResponse(lastMessage.id, response, message, user.id);
      return response;
    } catch (error) {
      throw error;
    }
  }

  /**
   * handle last message error
   * @param {Message} lastMessage last message object for error checking
   * @param {Error} error thrown error
   * @param {string} error description
   */
  checkLastMessageErrors(lastMessage, error) {
    console.log("chat_service:checkLastMessageErrors thrown = ", error);
    if (lastMessage.message === ASK_BIRTHDAY) {
      return "birth date format should be YYYY-MM-DD. ex: 1992-01-21";
    }
    return (
      "I cannot understand the response given, your last question was: " +
      lastMessage.message
    );
  }

  /**
   * create user
   * @param {string} uid facebook user id
   * @return {User} user model object
   */
  async createUser(uid) {
    return models.User.create({
      id: uid
    });
  }

  /**
   * check user birthdate, and response accordingly
   * @param {User} user
   * @return {string} bot response
   */
  async checkUserBirthdate(user) {
    if (user.birthDate) {
      // birthdate exist: ask calculation
      await models.Message.create({
        message: ASK_DAYS_UNTIL_BIRTHDAY,
        uid: user.id
      });
      return ASK_DAYS_UNTIL_BIRTHDAY;
    } else {
      // ask birthdate
      await models.Message.create({
        message: ASK_BIRTHDAY,
        uid: user.id
      });
      return ASK_BIRTHDAY;
    }
  }

  /**
   * return initial response
   * @param {string} uid facebook user id
   * @return {string} welcome message
   */
  async initialResponse(uid) {
    await models.Message.create({
      message: ASK_WELCOME_NAME,
      uid
    });
    return ASK_WELCOME_NAME;
  }

  /**
   * process message sent by user
   * @param {string} uid sender id
   * @param {string} message message sent by user
   * @return {string} response to user
   */
  async processMessage(uid, message) {
    const user = await models.User.findOne({
      where: { id: uid }
    });
    if (user) {
      const lastMessage = await this.getLastMessage(uid);
      if (lastMessage && !lastMessage.response) {
        try {
          return await this.replyLastMessage(lastMessage, message, user);
        } catch (error) {
          return this.checkLastMessageErrors(lastMessage, error);
        }
      } else {
        if (user.firstName) {
          return this.checkUserBirthdate(user);
        } else {
          //ask first name
          await models.Message.create({
            response: message,
            uid
          });
          return this.initialResponse(uid);
        }
      }
    } else {
      await this.createUser(uid);
      return this.initialResponse(uid);
    }
  }

  /**
   * check response to new user message
   * @param {User} user
   * @param {string} response
   * @param {string} prevMessage
   * @return {string} bot response
   */
  async checkResponse(user, response, prevMessage) {
    switch (prevMessage) {
      case ASK_WELCOME_NAME:
        await models.User.update(
          {
            firstName: response
          },
          {
            where: { id: user.id }
          }
        );
        return ASK_BIRTHDAY;
      case ASK_BIRTHDAY:
        if (validateDate(response)) {
          await models.User.update(
            {
              birthDate: response
            },
            {
              where: { id: user.id }
            }
          );
          return ASK_DAYS_UNTIL_BIRTHDAY;
        }
        throw new Error("invalid date format");
      case ASK_DAYS_UNTIL_BIRTHDAY:
        return this.checkUserCalculateUntilBirthdayResponse(response, user);
      case RESPONSE_BYE:
        return ASK_DAYS_UNTIL_BIRTHDAY;
      default:
        return ASK_DAYS_UNTIL_BIRTHDAY;
    }
  }

  /**
   * check user response on bot question to calculate days untill birthday
   * @param {string} response
   * @param {User} user model object
   * @return {string} response of bot
   */
  checkUserCalculateUntilBirthdayResponse(response, user) {
    const responseYes = ["yes", "y", "yup", "yeah", "ok", "yea"];
    const responseNo = ["nah", "no", "nope", "n"];
    if (responseYes.includes(response.toLowerCase())) {
      return `There are ${calculateDaysUntilBirthday(
        user.birthDate
      )} days until your next birthday.`;
    } else if (responseNo.includes(response.toLowerCase())) {
      return RESPONSE_BYE;
    }
    throw new Error("answer should be yes or no.");
  }
}

module.exports = ChatService;
