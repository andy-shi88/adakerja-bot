const { MessageService } = require("../services");
const ResponseBuilder = require("../utils/response_builder");
const BaseController = require("./base_controller");
const models = require("../models");

class MessageController extends BaseController {
  /**
   * Message controller class
   */
  constructor() {
    super(new MessageService());
  }

  async get(req, res) {
    const { page, limit, fields, order } = req.query;
    let responseBuilder = new ResponseBuilder();
    try {
      const result = await this.service.paginate(
        req,
        page,
        limit,
        order,
        fields,
        [
          {
            model: models.User,
            as: "user"
          }
        ]
      );

      this.sendSuccessResponse(
        res,
        responseBuilder
          .setData(result.data)
          .setLinks(result.links)
          .setTotal(result.total)
          .setCount(result.count)
          .setMessage("messages fetched successfully")
          .build()
      );
      return;
    } catch (error) {
      this.sendBadRequestResponse(
        res,
        responseBuilder
          .setSuccess(false)
          .setMessage(error.toString())
          .build()
      );
    }
  }

  async getByUid(req, res) {
    const { page, limit, fields, order } = req.query;
    const { uid } = req.params;
    let responseBuilder = new ResponseBuilder();
    try {
      const result = await this.service.paginate(
        req,
        page,
        limit,
        order,
        fields,
        [],
        {
          uid
        }
      );

      this.sendSuccessResponse(
        res,
        responseBuilder
          .setData(result.data)
          .setLinks(result.links)
          .setTotal(result.total)
          .setCount(result.count)
          .setMessage("messages fetched successfully")
          .build()
      );
      return;
    } catch (error) {
      this.sendBadRequestResponse(
        res,
        responseBuilder
          .setSuccess(false)
          .setMessage(error.toString())
          .build()
      );
    }
  }

  async find(req, res) {
    const { id } = req.params;
    let responseBuilder = new ResponseBuilder();
    try {
      const result = await this.service.findOne({ id });
      if (!result) {
        this.sendNotFoundResponse(
          res,
          responseBuilder
            .setSuccess(false)
            .setMessage("message not found")
            .build()
        );
        return;
      }
      this.sendSuccessResponse(
        res,
        responseBuilder
          .setData(result)
          .setMessage("message fetched successfully")
          .build()
      );
      return;
    } catch (error) {
      this.sendBadRequestResponse(
        res,
        responseBuilder
          .setSuccess(false)
          .setMessage(error.toString())
          .build()
      );
    }
  }

  async delete(req, res) {
    const { id } = req.params;
    let responseBuilder = new ResponseBuilder();
    try {
      await this.service.destroy({ id });
      this.sendSuccessResponse(
        res,
        responseBuilder.setMessage("message data deleted successfully").build()
      );
    } catch (error) {
      if (error.message === "data not found") {
        this.sendNotFoundResponse(
          res,
          responseBuilder
            .setSuccess(false)
            .setMessage("message not found")
            .build()
        );
        return;
      }
      this.sendInternalServerErrorResponse(
        res,
        responseBuilder
          .setMessage(
            "server error occured when deleting the message. Please try again later"
          )
          .setSuccess(false)
          .build()
      );
    }
  }
}

module.exports = MessageController;
