const FacebookController = require("./facebook_controller");
const MessageController = require("./message_controller");

/**
 * Controller singleton initialization
 */
exports.facebookController = new FacebookController();
exports.messageController = new MessageController();
