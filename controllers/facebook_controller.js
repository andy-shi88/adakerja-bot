const { FacebookService, ChatService } = require("../services");
const ResponseBuilder = require("../utils/response_builder");
const BaseController = require("./base_controller");
const { FACEBOOK_VERIFY_TOKEN, PAGE_ACCESS_TOKEN } = process.env;

class FacebookController extends BaseController {
  /**
   * Facebook controller class
   */
  constructor() {
    super(new FacebookService());
    this.chatService = new ChatService();
  }

  /**
   * auth hook to be accessed by facebook for verification
   * @param {any} req
   * @param {any} res
   * @return {mixed} return response to auth challenger
   */
  async authHook(req, res) {
    const {
      "hub.verify_token": hubToken,
      "hub.challenge": hubChallenge
    } = req.query;
    let responseBuilder = new ResponseBuilder();

    if (!hubToken) {
      this.sendInvalidPayloadResponse(
        res,
        responseBuilder
          .setSuccess(false)
          .setMessage("invalid token")
          .build()
      );
      return;
    }
    if (this.service.verifyToken(hubToken, FACEBOOK_VERIFY_TOKEN)) {
      return res.send(hubChallenge);
    }
    this.sendInvalidPayloadResponse(
      res,
      responseBuilder
        .setSuccess(false)
        .setMessage("invalid token")
        .build()
    );
  }

  async messageHandler(req, res) {
    if (req.body.object === "page") {
      req.body.entry.forEach(entry => {
        entry.messaging.forEach(async event => {
          const { sender } = event;
          if (event.message && event.message.text) {
            const response = await this.chatService.processMessage(
              sender.id,
              event.message.text
            );
            this.service.sendMessage(sender.id, response, PAGE_ACCESS_TOKEN);
          }
        });
      });
      res.status(200).end();
    }
  }
}

module.exports = FacebookController;
