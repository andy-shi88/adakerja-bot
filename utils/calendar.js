const moment = require("moment");

/**
 * calculate days left until next birthday
 * @param {string} birthdate  format: {YYYY-MM-DD}
 * @return number of days until next birthday
 */
const calculateDaysUntilBirthday = birthdate => {
  const birthday = moment(birthdate).year(moment().year());
  const today = moment().format("YYYY-MM-DD");
  if (birthday.isSame(today)) {
    return 0;
  } else {
    let daysLeft = birthday.diff(today, "days");
    if (daysLeft < 0) {
      daysLeft = birthday.year(moment().year() + 1).diff(today, "days");
    }
    return daysLeft;
  }
};

/**
 * validate user date input
 * @param {string} date date string with YYYY-MM-DD format
 * @return {boolean} date validation result
 */
const validateDate = date => {
  return moment(date, "YYYY-MM-DD", true).isValid();
};

exports.calculateDaysUntilBirthday = calculateDaysUntilBirthday;
exports.validateDate = validateDate;
