### AdaKerja Interview 2 Test

#### environment used:

- node: v8.11.4
- local hook environment:

  - ngrok: version 2.2.8
    - mac installation: https://gist.github.com/wosephjeber/aa174fb851dfe87e644e

* run `npm install` to install all dependencies needed in the project.
* copy `.env.example` and modify as needed. -> `cp .env.example .env`
* run `node index.js` to run the development server.

#### persistent storage preparation

- create database called `adakerja-messenger` with `utf8mb4` charset (to store emoji)
- configure db setting at `config/config.json`
- run db schema migration `./node_modules/.bin/sequelize db:migrate`

#### Tests

- run unit test by `npm test`
- run controller test provided through postman collection by importing from your postman app.
