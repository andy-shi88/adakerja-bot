const express = require("express");
const { messageController } = require("../controllers");

const router = express.Router();

router.get("/", (req, res) => {
  messageController.get(req, res);
});

router.get("/:id", (req, res) => {
  messageController.find(req, res);
});

router.delete("/:id", (req, res) => {
  messageController.delete(req, res);
});

module.exports = router;
