const express = require("express");
const { messageController } = require("../controllers");

const router = express.Router();

router.get("/:uid/messages", (req, res) => {
  messageController.getByUid(req, res);
});

module.exports = router;
