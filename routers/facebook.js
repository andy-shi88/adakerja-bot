const express = require("express");
const { facebookController } = require("../controllers");

const router = express.Router();

router.get("/webhook", (req, res) => {
  facebookController.authHook(req, res);
});

router.post("/webhook", (req, res) => {
  facebookController.messageHandler(req, res);
});

module.exports = router;
