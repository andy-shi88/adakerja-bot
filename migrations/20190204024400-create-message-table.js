"use strict";
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface
      .createTable("messages", {
        id: {
          allowNull: false,
          autoIncrement: true,
          primaryKey: true,
          type: Sequelize.INTEGER
        },
        uid: {
          allowNull: false,
          type: Sequelize.STRING,
          references: {
            model: "users",
            key: "id"
          },
          field: "uid"
        },
        message: {
          allowNull: true,
          type: Sequelize.STRING,
          field: "message"
        },
        response: {
          type: Sequelize.STRING,
          allowNull: true,
          field: "response"
        },
        createdAt: {
          allowNull: false,
          type: Sequelize.DATE,
          field: "created_at"
        },
        updatedAt: {
          allowNull: false,
          type: Sequelize.DATE,
          field: "updated_at"
        }
      })
      .then(() => queryInterface.addIndex("messages", ["uid"]));
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable("messages");
  }
};
