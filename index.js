const express = require("express");
const bodyParser = require("body-parser");
//load dotenv
require("dotenv").config();

const facebook = require("./routers/facebook");
const message = require("./routers/message");
const user = require("./routers/user");

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use("/facebook", facebook);
app.use("/messages", message);
app.use("/users", user);

app.listen(5000, () => console.log("Bot running on port 5000"));
