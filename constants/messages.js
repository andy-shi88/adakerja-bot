/**
 * message template
 */

exports.ASK_WELCOME_NAME = "Hi, what's your name?";
exports.ASK_BIRTHDAY =
  "when is your birthday? (format: YYYY-MM-DD, ex: 1994-02-23)";
exports.ASK_DAYS_UNTIL_BIRTHDAY =
  "want me to calculate how many days till your birthday?";
exports.RESPONSE_BYE = "Goodbye 👋";
