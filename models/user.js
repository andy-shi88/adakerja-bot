"use strict";

module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define(
    "User",
    {
      id: {
        type: DataTypes.STRING,
        unique: {
          args: true,
          msg: "user id exist"
        },
        allowNull: false,
        validate: {
          notEmpty: { msg: "Please input user id" }
        },
        primaryKey: true
      },
      firstName: {
        type: DataTypes.STRING,
        allowNull: true,
        field: "first_name"
      },
      birthDate: {
        type: DataTypes.DATEONLY,
        allowNull: true,
        field: "birth_date"
      }
    },
    {
      createdAt: "created_at",
      updatedAt: "updated_at",
      timestamps: true,
      underscored: true,
      tableName: "users"
    }
  );
  User.associate = function(models) {};
  return User;
};
