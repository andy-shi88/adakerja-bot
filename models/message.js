"use strict";

module.exports = (sequelize, DataTypes) => {
  const Message = sequelize.define(
    "Message",
    {
      message: {
        type: DataTypes.STRING,
        allowNull: true,
        field: "message"
      },
      response: {
        type: DataTypes.STRING,
        allowNull: true,
        field: "response"
      },
      uid: {
        type: DataTypes.STRING,
        allowNull: true,
        references: {
          model: "users",
          key: "id"
        },
        field: "uid"
      }
    },
    {
      createdAt: "created_at",
      updatedAt: "updated_at",
      timestamps: true,
      underscored: true,
      tableName: "messages"
    }
  );
  Message.associate = function(models) {
    Message.belongsTo(models.User, {
      as: "user",
      foreignKey: "uid"
    });
  };
  return Message;
};
