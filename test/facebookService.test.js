const { FacebookService } = require("../services");

const facebookService = new FacebookService();

const assert = require("assert");

describe("facebookservice", function() {
  describe("verifyToken", function() {
    it("should return true for correct token access", function() {
      assert.equal(
        facebookService.verifyToken("DUMMY_TOKEN", "DUMMY_TOKEN"),
        true
      );
    });
    it("should return false for incorrect token access", function() {
      assert.equal(
        facebookService.verifyToken("DUMMY_TOKEN", "FALSE_DUMMY_TOKEN"),
        false
      );
    });
  });
});
