const {
  ASK_WELCOME_NAME,
  ASK_BIRTHDAY,
  ASK_DAYS_UNTIL_BIRTHDAY,
  RESPONSE_BYE
} = require("../constants/messages");
const sinon = require("sinon");
const { ChatService } = require("../services");
const models = require("../models");
const { assert } = require("chai");
const chatService = new ChatService();

let userFindOneStub = sinon
  .stub(models.User, "findOne")
  .callsFake(condition => {
    return undefined;
  });
userFindOneStub
  .onFirstCall()
  .returns(undefined)
  .onSecondCall()
  .returns({
    id: "test-id",
    firstName: null,
    birthDate: null
  })
  .onThirdCall()
  .returns({
    id: "test-id",
    firstName: "John",
    birthDate: null
  });

sinon.stub(models.User, "update").callsFake((payload, condition) => {
  return {};
});

sinon.stub(models.User, "create").callsFake((payload, condition) => {
  return {};
});

messageFindOneStub = sinon
  .stub(models.Message, "findOne")
  .callsFake(condition => {
    return undefined;
  });

sinon.stub(models.Message, "create").callsFake(payload => {
  return {};
});

sinon.stub(models.Message, "update").callsFake(payload => {
  return {};
});

describe("chatService", function() {
  describe("chatService", function() {
    it("should be an object", function() {
      assert.isObject(chatService, "chatService is an object");
    });
  });

  describe("processMessage : first timer user", function() {
    it(`should return ${ASK_WELCOME_NAME}`, async function() {
      const response = await chatService.processMessage("uid", "Hi");
      assert.equal(response, ASK_WELCOME_NAME);
    });
  });

  describe("processMessage : user inserted without firstname yet", function() {
    it(`should return ${ASK_WELCOME_NAME}`, async function() {
      const response = await chatService.processMessage("uid", "Hi");
      assert.equal(response, ASK_WELCOME_NAME);
    });
  });

  describe("processMessage : user with firstname", function() {
    it(`should return ${ASK_BIRTHDAY}`, async function() {
      const response = await chatService.processMessage("uid", "Hi");
      assert.equal(response, ASK_BIRTHDAY);
    });
  });

  describe("processMessage : user with firstname and birthday", function() {
    it(`should return ${ASK_DAYS_UNTIL_BIRTHDAY}`, async function() {
      userFindOneStub.reset();
      userFindOneStub.returns({
        id: "test-id",
        firstName: "John",
        birthDate: "1978-02-16"
      });
      const response = await chatService.processMessage("uid", "Hi");
      assert.equal(response, ASK_DAYS_UNTIL_BIRTHDAY);
    });
  });

  describe("processMessage : user dont want to know days until birthday", function() {
    it(`should return ${RESPONSE_BYE}`, async function() {
      messageFindOneStub.reset();
      messageFindOneStub.returns({
        message: ASK_DAYS_UNTIL_BIRTHDAY
      });
      const response = await chatService.processMessage("uid", "No");
      assert.equal(response, RESPONSE_BYE);
    });
  });

  describe("processMessage : user want to know days until birthday", function() {
    it(`should include 'days until your next birthday.'`, async function() {
      messageFindOneStub.reset();
      messageFindOneStub.returns({
        message: ASK_DAYS_UNTIL_BIRTHDAY
      });
      const response = await chatService.processMessage("uid", "Yes");
      assert.deepInclude(response, "days until your next birthday.");
    });
  });

  describe("processMessage : user come back after knowing days until their birthday", function() {
    it(`should ask again: ${ASK_DAYS_UNTIL_BIRTHDAY}.'`, async function() {
      messageFindOneStub.reset();
      messageFindOneStub.returns({
        message: "There are 331 days until your next birthday."
      });
      const response = await chatService.processMessage("uid", "Hi");
      assert.deepInclude(response, ASK_DAYS_UNTIL_BIRTHDAY);
    });
  });

  describe("checkLastMesssageErrors", function() {
    it(`should return 'birth date format should be YYYY-MM-DD. ex: 1992-01-21' if last message is ${ASK_BIRTHDAY}`, async function() {
      assert(
        chatService.checkLastMessageErrors({ message: ASK_BIRTHDAY }, {}),
        "birth date format should be YYYY-MM-DD. ex: 1992-01-21"
      );
    });
    it(`should return error object if last message is not ${ASK_BIRTHDAY}`, async function() {
      const errors = { errors: "any error" };
      assert.include(
        chatService.checkLastMessageErrors(
          { message: ASK_WELCOME_NAME },
          errors
        ),
        `I cannot understand the response given, your last question was: ${ASK_WELCOME_NAME}`
      );
    });
  });

  describe("checkUserBirthdate", function() {
    it(`should return "${ASK_BIRTHDAY}" when user.birthdate is empty`, async function() {
      assert.equal(
        await chatService.checkUserBirthdate({ birthDate: undefined }),
        ASK_BIRTHDAY
      );
    });
    it(`should return "${ASK_DAYS_UNTIL_BIRTHDAY}" if birthdate is filled`, async function() {
      assert.equal(
        await chatService.checkUserBirthdate({ birthDate: "1992-01-01" }),
        ASK_DAYS_UNTIL_BIRTHDAY
      );
    });
  });

  describe("initialResponse", function() {
    it(`should return "${ASK_WELCOME_NAME}"`, async function() {
      assert.equal(await chatService.initialResponse(), ASK_WELCOME_NAME);
    });
  });

  describe("checkUserCalculateUntilBirthdayResponse", function() {
    it(`should return include "...days until your next birthday." if response include "yes", "y", "yup", "yeah", "ok", "yea"`, async function() {
      const positiveResponse = ["yes", "y", "yup", "yeah", "ok", "yea"];
      positiveResponse.map(async res => {
        assert.include(
          await chatService.checkUserCalculateUntilBirthdayResponse(res, {
            birthDate: "1992-02-01"
          }),
          " days until your next birthday."
        );
      });
    });

    it(`should return include "${RESPONSE_BYE}" if response include "nah", "no", "nope", "n"`, async function() {
      const negativeResponse = ["nah", "no", "nope", "n"];
      negativeResponse.map(async res => {
        assert.equal(
          await chatService.checkUserCalculateUntilBirthdayResponse(res, {
            birthDate: "1992-02-01"
          }),
          RESPONSE_BYE
        );
      });
    });

    it(`should throw (answer should be yes or no.) if answer is not recognized`, async function() {
      assert.throws(
        function() {
          chatService.checkUserCalculateUntilBirthdayResponse(
            "random response",
            {
              birthDate: "1992-02-01"
            }
          );
        },
        Error,
        "answer should be yes or no."
      );
    });

    it(`should throw (Cannot read property 'toLowerCase' of undefined) if answer is undefined`, async function() {
      assert.throws(
        function() {
          chatService.checkUserCalculateUntilBirthdayResponse(undefined, {
            birthDate: "1992-02-01"
          });
        },
        Error,
        "Cannot read property 'toLowerCase' of undefined"
      );
    });
  });

  describe("checkResponse", function() {
    it(`should return "${ASK_BIRTHDAY}" if prevMessage = "${ASK_WELCOME_NAME}"`, async function() {
      assert.equal(
        await chatService.checkResponse(
          { id: "123" },
          "RANDOMNAME",
          ASK_WELCOME_NAME
        ),
        ASK_BIRTHDAY
      );
    });

    it(`should return "${ASK_DAYS_UNTIL_BIRTHDAY}" if prevMessage = "${ASK_BIRTHDAY}" and response is valid date`, async function() {
      assert.equal(
        await chatService.checkResponse(
          { id: "123" },
          "1998-02-01",
          ASK_BIRTHDAY
        ),
        ASK_DAYS_UNTIL_BIRTHDAY
      );
    });

    it(`should return "${ASK_DAYS_UNTIL_BIRTHDAY}" if prevMessage = "${RESPONSE_BYE}"`, async function() {
      assert.equal(
        await chatService.checkResponse(
          { id: "123" },
          "1998-02-01",
          RESPONSE_BYE
        ),
        ASK_DAYS_UNTIL_BIRTHDAY
      );
    });

    it(`should return include "...days until your next birthday." if prevMessage = "${ASK_DAYS_UNTIL_BIRTHDAY}" and answer yes`, async function() {
      assert.include(
        await chatService.checkResponse(
          { id: "123" },
          "yes",
          ASK_DAYS_UNTIL_BIRTHDAY
        ),
        " days until your next birthday."
      );
    });

    it(`should return include "${RESPONSE_BYE}" if prevMessage = "${ASK_DAYS_UNTIL_BIRTHDAY}" and answer no`, async function() {
      assert.equal(
        await chatService.checkResponse(
          { id: "123" },
          "no",
          ASK_DAYS_UNTIL_BIRTHDAY
        ),
        RESPONSE_BYE
      );
    });
  });
});
