const assert = require("assert");
const moment = require("moment");
const calendar = require("../utils/calendar");

describe("calendar", function() {
  describe("calculateDaysUntilBirthday", function() {
    it("should return 0 if birthday is today", function() {
      assert.equal(calendar.calculateDaysUntilBirthday(moment()), 0);
    });
    it("should return number of days to the next birthday [1]", function() {
      assert.equal(
        calendar.calculateDaysUntilBirthday(moment().add(1, "days")),
        1
      );
    });
    it("should return number of days to the next year birthday, if birthday have passed", function() {
      assert.notEqual(
        calendar.calculateDaysUntilBirthday(moment().subtract(1, "days")),
        1
      );
    });
  });
  describe("validateDate", function() {
    it("should return true for correct date format (YYYY-MM-DD)", function() {
      assert.equal(calendar.validateDate("1997-01-30"), true);
    });
    it("should return false for correct date format (YYYY-MM-DD) but incorrect date (30 february 1997)", function() {
      assert.equal(calendar.validateDate("1997-02-30"), false);
    });
    it("should return false for incorrect date format (DD-MM-YYYY)", function() {
      assert.equal(calendar.validateDate("02-02-1997"), false);
    });
    it("should return false for any other input (incorrect date format or incorrect value)", function() {
      assert.equal(calendar.validateDate("foo"), false);
    });
  });
});
