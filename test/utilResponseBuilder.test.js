const assert = require("assert");
const ResponseBuilder = require("../utils/response_builder");

describe("ResponseBuilder", function() {
  let responseBuilder = new ResponseBuilder();
  describe("responseBuilder", function() {
    const emptyResponse = {
      meta: {
        message: "operations success",
        success: true,
        errors: []
      },
      links: null,
      data: {},
      include: null
    };
    it("should be not be an empty object of ResponseBuilder", function() {
      assert.notEqual(responseBuilder, emptyResponse);
    });
  });
  describe("build", function() {
    const filledResponse = {
      meta: {
        message: "operations success",
        success: true
      },
      data: { name: "John Doe" }
    };
    it("should return filled Response object", function() {
      const result = responseBuilder
        .setData({ name: "John Doe" })
        .setSuccess(true)
        .setMessage("operations success")
        .build();
      assert.notStrictEqual(result, filledResponse);
    });
  });
});
